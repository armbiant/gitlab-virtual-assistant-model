import argparse
from comet_ml import API
import os
import postprocess


def get_args():
    """Gets arguments passed in command line"""

    parser = argparse.ArgumentParser()
    parser.add_argument("--workspace", type=str)
    parser.add_argument("--project_name", type=str)
    parser.add_argument("--model_name", type=str)

    return parser.parse_args()


def increment_minor_version(version):
    """Gets and returns increment in model version"""

    version = version.split(".")
    version[1] = str(int(version[1]) + 1)

    return ".".join(version)


def get_experiment_id(workspace, project_name, model_name):
    """Fetch the id of the latest experiment that logged a model"""

    try:
        api = API(api_key=os.environ["COMET_API_KEY"])
    except:
        print("Using API Key local")
        api = API()
    experiments = api.get(str(workspace), str(project_name))

    # Filter only the experiments that logged a model
    model_experiments = [
        experiment
        for experiment in experiments
        if experiment.get_model_asset_list(model_name)
    ]
    # Sort Experiments based on Timestamp
    model_experiments = sorted(
        model_experiments, key=lambda x: x.end_server_timestamp, reverse=True
    )

    latest_model_experiment = model_experiments[0]
    experiment_id = latest_model_experiment.id

    return experiment_id


def register_model(workspace, project_name, model_name):
    """Register model in Comet Server"""

    api = API()

    model_versions = api.get_registry_model_versions(workspace, model_name)

    # Get the current version of the model in the registry
    latest_version = model_versions[-1]
    model_details = api.get_registry_model_details(
        workspace, model_name, version=latest_version
    )
    latest_model_experiment_id = model_details["experimentModel"]["experimentKey"]
    experiment_id = postprocess.get_experiment_id(workspace, project_name, model_name)

    if latest_model_experiment_id == experiment_id:
        print(f"Model has already been registered as {model_name}:{latest_version}")
        return

    new_version = increment_minor_version(latest_version)
    experiment = api.get(f"{workspace}/{project_name}/{experiment_id}")
    experiment.register_model(model_name=model_name, version=new_version)


def main():
    """Gets arguments from command line and register model in comet"""

    args = get_args()
    workspace = args.workspace
    project_name = args.project_name
    model_name = args.model_name
    register_model(workspace, project_name, model_name)



if __name__ == "__main__":
    main()
