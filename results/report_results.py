import json
from pytablewriter import MarkdownTableWriter

intent_report = json.load(open('intent_report.json'))
gral_values = ['accuracy', 'macro avg', 'weighted avg']
intent_keys = list(intent_report)
intent_keys = [intent for intent in intent_keys if intent not in gral_values]
intent_metric = [intent_report[intent] for intent in intent_keys]
metric_keys = list(intent_metric[0].keys())
# Table report
writer = MarkdownTableWriter()
writer.table_name = "Intents Performance Report- NLU"
writer.headers = ["Intent"]+metric_keys
writer.value_matrix = [[intent_keys[index]]+[intent_report[o_key][i_key] for i_key in metric_keys] for index, o_key in enumerate(intent_keys,0)]
writer.margin = 1  # add a whitespace for both sides of each cell
writer.dump('intent_report.md')
print("script executed table created")
